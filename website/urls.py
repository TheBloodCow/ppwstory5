from django.urls import path
from django.conf.urls import url
from .views import profile, registration, addEvent,saveproject
#url for app
urlpatterns = [

    path('', profile, name ='profile'),
	path('registration', registration, name='registration'),
    path('addEvent', addEvent, name = 'addEvent'),  

    path('save_project', saveproject, name ="saveproject")
]
