from django.db import models
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.
class PersonalSchedule(models.Model):
    date = models.DateTimeField(default=timezone.now);
    name = models.CharField(max_length=40);
    location = models.CharField(max_length=40);
    category = models.CharField(max_length=40);
