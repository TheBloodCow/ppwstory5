from django import forms
from .models import PersonalSchedule
import datetime

class Add_Event(forms.Form):
    error_messages = {
        'required': 'Please type',
    }
    pos_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Your name'
    }
    title_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Your Location'
    }
    description_attrs = {
        'type': 'text',
        'class': 'todo-form-input',
        'placeholder':'Your Category'
    }

    name = forms.CharField(label='', required=True, max_length=40, widget=forms.TextInput(attrs=pos_attrs))
    location = forms.CharField(label='', required=True, max_length=40, widget=forms.TextInput(attrs=title_attrs))
    category = forms.CharField(label='', required=True, max_length=40, widget=forms.TextInput(attrs=description_attrs))
    date = forms.DateField(initial=datetime.date.today)
