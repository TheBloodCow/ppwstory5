from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import Add_Event
from .models import PersonalSchedule

# Create your views here.
def profile(request):
	project = PersonalSchedule.objects.all()
	print("ads")
	print(project)
	response['project'] = project
	return render(request, "listLess.html",response)
def registration(request):

	return render(request, "Registration.html")
def addEvent(request):
	return render(request, "addEvent.html")

response = {}

def index(request):#!--duplicate-->
	print("asd")
	project = PersonalSchedule.objects.all()
	print(project)
	response['project'] = project

	html = 'listLess.html'
	return render(request, html, response)
#will refer to the normal webpage
def addEvent(request):
	response['title'] = 'Add Event to Calendar'
	response['addEvent_form'] = Add_Event()

	html = 'addEvent.html'

	return render(request, html, response)


#will respond with this html upon a request for add_event
def saveproject(request):
	form = Add_Event(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['name'] = request.POST['name']
		response['location'] = request.POST['location']
		response['category'] = request.POST['category']
		project = PersonalSchedule(name=response['name'], location=response['location'],category=response['category'])
		project.save()
		print("sukses")
		return HttpResponseRedirect(reverse('website:profile'))
	else:
		return HttpResponseRedirect('/')
#This is to save said information that has been stored
